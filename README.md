## hg38make: a makefile to build a GATK bundle in genome build 38.

The [GATK] and GATK bundle are ubiqitous in the analysis of NGS data. The current
release, however is based on human genome build 37 (GRCh37).
The latest genome build, GRCh38, has been available for a couple of years now
and this makefile facilitates fetching/building the GATK bundle files
for GRCh38.

## Quick start:
* install [VCFtools][VCFTOOLS] and [CrossMap][CROSSMAP]
* run "make -f hg38.makefile PREP" and "make -f hg38.makefile BUNDLE"


## Prerequisites

[CrossMap][CROSSMAP] and [VCFtools][VCFTOOLS] must be installed.


## Tools

The makefile uses [**bwa.kit**][BWA.KIT] to fetch the genome sequence, decoy sequences
and ALT sequences. See [this link][BWA_ALT] for info.
Then it downloads the files in the GATK bundle, necessary for running the GATK
best practices pipeline (various VCF files for BQSR/VQSR).
[CrossMap][CROSSMAP] is used to map/lift the vcf files over to the latest genome
build. [VCFtools][VCFTOOLS] are used to sort the resulting vcf files, as coordinates may
have shifted during the mapping process. Finally, it is possible that alleles may have
changed during the mapping process, resulting in the REF allele in the vcf file being
one of the ALT alleles as well. GATK dislikes this and therefore we remove those
entries.






[GATK]: https://www.broadinstitute.org/gatk/
[BWA.KIT]: https://github.com/lh3/bwa/tree/master/bwakit
[BWA_ALT]: https://github.com/lh3/bwa/blob/master/README-alt.md
[CROSSMAP]: http://crossmap.sourceforge.net/
[VCFTOOLS]: http://vcftools.sourceforge.net/