###
# hg38.makefile, 
#

###
# Assignments - set DEFAULT_CHAIN and DEFAULT_REF on cl to override
#

B38FASTA:=refs/Homo_sapiens.GRCh38.dna.primary_assembly.fa
B38TOPLEVEL:=refs/Homo_sapiens.GRCh38.dna.toplevel.fa
HS38DH:=hs38DH.fa
hs38:=hs38
hs38a:=hs38a
hs38DH:=hs38DH

REF:=$(hs38DH)
REFDIR=bundle/2.8/$(REF)
REFPATH=bundle/2.8/$(REF)/$(REF).fa
REFINDEX=bundle/2.8/$(REF)/$(REF).fa.bwt
REFSAMIDX=$(REFPATH).fai

CROSSMAP:=CrossMap.py
CHAIN19TO38:=chains/hg19ToHg38.over.chain.gz
CHAINb37TOb38:=chains/GRCh37_to_GRCh38.edit.chain.gz
CHAINHG19TOHG38:=chains/hg19ToHg38.over.chain.gz
DEFAULT_CHAIN:=$(CHAINHG19TOHG38)

###
# sorting is now done by picard, so we need path to picard.jar
# also we need java 1.7 - this is starting to get very uppmax specific and ugly
# edit the paths to java and picard on your system here, or
# specify on the command line when running e.g. "make ... JAVA=/path/to/java"
PICARD:=/sw/apps/bioinfo/picard/2.0.1/milou/picard.jar
JAVA:=/sw/comp/java/x86_64/sun_jdk1.8.0_40/bin/java


VPATH=:./bwa.kit:./bundle/2.8/b37


###
# Default usage and help
#

USAGE :
	@printf "\nConvenience makefile to create a GATK bundle in genome build hg38\n\n"
	@printf "Quick start: install CrossMap (http://crossmap.sourceforge.net/)\n"
	@printf "install vcftools (http://vcftools.sourceforge.net)\n"
	@printf "and run: make PREP && make BUNDLE\n\n"
	@printf "The pipeline uses bwa.kit (https://github.com/lh3/bwa/blob/master/README-alt.md)\n"
	@printf "to download and construct the latest reference assembly with decoy and ALT sequences.\n"
	@printf "It then downloads (parts of) the GATK bundle (build b37) and uses CrossMap to map\n"
	@printf "files over to the latest build.\n\n"


PREP : crosscheck vcfcheck $(REFPATH) gatk_mini_bundle hg19ToHg38.over.chain.gz
	@printf "\nData files ready, now run make BUNDLE\n"

BUNDLE : Mills_and_1000G_gold_standard.indels.b38.nodup.sort.vcf 1000G_phase1.indels.b38.nodup.sort.vcf dbsnp_138.b38.nodup.sort.vcf hapmap_3.3.b38.nodup.sort.vcf 1000G_omni2.5.b38.nodup.sort.vcf 1000G_phase1.snps.high_confidence.b38.nodup.sort.vcf
	mkdir -p $(REFDIR)
	mv Mills_and_1000G_gold_standard.indels.b38.nodup.sort.vcf 1000G_phase1.indels.b38.nodup.sort.vcf dbsnp_138.b38.nodup.sort.vcf hapmap_3.3.b38.nodup.sort.vcf 1000G_omni2.5.b38.nodup.sort.vcf 1000G_phase1.snps.high_confidence.b38.nodup.sort.vcf $(REFDIR)
	@printf "\nVCF files ready. Use *nodup.sort.vcf files in $(REFDIR)\n"

BWAINDEX : $(REFINDEX)
	@printf "bwa indexing done\n\n"

###
# bwa related stuff - download and use script in there to build the ref w decoys etc.
#

BWA_KIT_DL:=http://sourceforge.net/projects/bio-bwa/files/bwakit/bwakit-0.7.13_x64-linux.tar.bz2/download

bwa.kit :
	wget -O- $(BWA_KIT_DL)  | tar xjf -

refs/hs38DH.fa refs/hs38DH.fa.alt: bwa.kit
	mkdir -p refs
	/bin/bash ./bwa.kit/run-gen-ref hs38DH && mv hs38DH.fa hs38DH.fa.alt refs

refs/hs38DH.fa.amb : refs/hs38DH.fa
	./bwa.kit/bwa index $<


$(REFPATH) : bwa.kit
	mkdir -p $(REFDIR)
	/bin/bash ./bwa.kit/run-gen-ref $(REF)
	cat $(REF).fa | perl -ne 's/^>chr/>/; print;' > $(REF).fa.tmp && mv $(REF).fa.tmp $(REF).fa && mv $(REF).fa $(REFDIR)
	if [ -e $(REF).fa.alt ]; then mv $(REF).fa.alt $(REFDIR); fi

%.fa.sa %.fa.bwt %.fa.pac %.fa.ann %.fa.amb : %.fa
	./bwa.kit/bwa index $<

###
# gatk bundle, download via ftp
#

GATK_FTP:=ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/2.8/b37/
BUNDLE_DIR:=bundle/2.8/b37
VCF_INPUT_FILES:=Mills_and_1000G_gold_standard.indels.b37.vcf.gz 1000G_phase1.indels.b37.vcf.gz dbsnp_138.b37.vcf.gz hapmap_3.3.b37.vcf.gz 1000G_omni2.5.b37.vcf.gz 1000G_phase1.snps.high_confidence.b37.vcf.gz Mills_and_1000G_gold_standard.indels.b37.vcf.idx.gz 1000G_phase1.indels.b37.vcf.idx.gz dbsnp_138.b37.vcf.idx.gz hapmap_3.3.b37.vcf.idx.gz 1000G_omni2.5.b37.vcf.idx.gz 1000G_phase1.snps.high_confidence.b37.vcf.idx.gz


gatk_bundle : 
	echo -e "This download is 30g !!!\n\n"
	wget -r -N -nd -P $(BUNDLE_DIR) $(GATK_FTP)

gatk_mini_bundle :
	mkdir -p $(BUNDLE_DIR)
	$(foreach FILE, $(VCF_INPUT_FILES), wget -N -P $(BUNDLE_DIR) $(GATK_FTP)/$(FILE); )

#$(VCF_INPUT_FILES) : gatk_mini_bundle 

####
# crossmap & chain files installation
#


.DUMMY : crosscheck vcfcheck
crosscheck :
	@which CrossMap.py || echo "CrossMap.py not found in PATH"
	@which CrossMap.py

vcfcheck : 
	@which vcftools || echo "VCFtools not found in PATH"
	@which vcftools

hg19ToHg38.over.chain.gz :
	mkdir -p chains
	wget -P chains http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz

GRCh37_to_GRCh38.chain.gz :
	mkdir -p chains
	wget -P chains ftp://ftp.ensembl.org/pub/assembly_mapping/homo_sapiens/GRCh37_to_GRCh38.chain.gz

GRCh37_to_GRCh38.edit.chain.gz : GRCh37_to_GRCh38.chain.gz
	zcat $< | perl -ane 'if ($$F[0] eq "chain"){if ($$F[7] =~ m/^CHR/){print;} else {$$F[7] = "chr" . $$F[7]; print join(" ", @F), "\n"}} else {print};' | gzip > $@.tmp && mv $@.tmp $@

###
# ensembl related stuff - will probably move away from this
#

Homo_sapiens.GRCh38.dna.primary_assembly.fa : 
	mkdir -p refs
	wget -N -P refs ftp://ftp.ensembl.org/pub/release-79/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
Homo_sapiens.GRCh38.dna.toplevel.fa :
	mkdir -p refs
	wget -N -P refs ftp://ftp.ensembl.org/pub/release-79/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.toplevel.fa.gz



###
# crossmapping targets:
#
# CrossMap does not update the contig info in the vcf header, so it will fail 
# GATK assertions, need to fix, grab the contig names and lengths from REFPATH.fai
#
# Then we need to sort with Picard.jar SortVcf, as vcf-sort cannot handle
# sorting by header dictionary ! BUT: the idx file created by picard is broken, so we need
# to remove that, once created !
#


%.fa.fai : %.fa
	samtools faidx $<

###
# grab occurrence of first contig line
# push the first N lines into new file
# then the "new" contigs list
# grab rest of file except contig stuff:
#
%.b38.vcf : %.b37.vcf $(DEFAULT_CHAIN) $(REFPATH) $(REFSAMIDX)
	$(CROSSMAP) vcf $(DEFAULT_CHAIN) $< $(REFPATH) $@.tmp
	CTGSTART=$$(cat $@.tmp | egrep -n -m 1 "^##contig|^#CHROM" | cut -d":" -f1) && \
	head -$$((CTGSTART -1)) $@.tmp > $@.tmp.tmp && \
	cat $(REFSAMIDX) | perl -ane 'print "##contig=<ID=", $$F[0], ",length=", $$F[1], ",assembly=hg38>\n"' >> $@.tmp.tmp && \
	tail -n +$$CTGSTART $@.tmp | grep -v "^##contig" >> $@.tmp.tmp && \
	mv $@.tmp.tmp $@
	rm -f $@.tmp.unmap $@.tmp

###
# idx file is broken when header-dict-sorting with Picard - remove
#
%.sort.vcf : %.vcf
	$(JAVA) -jar $(PICARD) SortVcf  I=$< O=$@.tmp.vcf && mv $@.tmp.vcf $@
	rm -f $@.tmp.vcf.idx

#%.sort.vcf : %.vcf
#	vcf-sort -c $< > $@.tmp && mv $@.tmp $@

###
# remove sites where ALT=REF and wherever there is IUPAC code instead of actual ACGT
# otherwise GATK will complain:
#
%.nodup.vcf : %.vcf
	 cat $< | perl -ane 'if (/^\#/){print; next;}; $$dupFlag=0; @alts=split(/\,/,$$F[4]); foreach $$alt (@alts){if ($$alt eq $$F[3]){$$dupFlag=1;}} if ($$F[3] =~ /[RWBKY]/){$$F[3] = "N"} print join("\t", @F), "\n" unless ($$dupFlag);' > $@.tmp && mv $@.tmp $@

%.vcf : %.vcf.gz
	gunzip -c $< > $@.tmp && mv $@.tmp $@


